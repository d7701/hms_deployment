-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: hospital_management
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `a_id` int NOT NULL AUTO_INCREMENT,
  `a_firstName` varchar(20) DEFAULT NULL,
  `a_lastName` varchar(20) DEFAULT NULL,
  `a_email` varchar(30) DEFAULT NULL,
  `a_password` varchar(600) DEFAULT NULL,
  `a_contact` varchar(15) DEFAULT NULL,
  `a_otp` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Rashmi','S','rashmisinha2601@gmail.com','$2a$10$FTCKSmFa.A4Iih5Wy8b1ZONckPnBuNKZq.y6NrIa7sPlnQta0yzKK','123456789','78091');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment` (
  `ap_id` int NOT NULL AUTO_INCREMENT,
  `p_id` int DEFAULT NULL,
  `ap_date` date DEFAULT NULL,
  `ap_time` time DEFAULT NULL,
  `p_admitStatus` bit(1) DEFAULT NULL,
  `p_admitRecommendationStatus` bit(1) DEFAULT NULL,
  `p_treatment_status` bit(1) DEFAULT NULL,
  `p_ap_status` bit(1) DEFAULT NULL,
  `ap_reason` varchar(500) DEFAULT NULL,
  `d_id` int DEFAULT NULL,
  PRIMARY KEY (`ap_id`),
  KEY `p_id` (`p_id`),
  KEY `d_id` (`d_id`),
  CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `patient` (`p_id`),
  CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`d_id`) REFERENCES `doctor` (`d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES (1,1,'1991-06-03','12:03:04',_binary '',_binary '',_binary '',_binary '','HeadAche',1),(2,1,'1991-06-01','12:03:04',_binary '\0',_binary '',_binary '\0',_binary '','Fever',1),(3,2,'1990-06-03','12:03:04',_binary '\0',_binary '',_binary '\0',_binary '','motion',1);
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bill` (
  `bill_id` int NOT NULL AUTO_INCREMENT,
  `ap_id` int DEFAULT NULL,
  `discharge_date` date DEFAULT NULL,
  `room_id` int DEFAULT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `ap_id` (`ap_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `bill_ibfk_1` FOREIGN KEY (`ap_id`) REFERENCES `appointment` (`ap_id`),
  CONSTRAINT `bill_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (3,1,'2022-02-01',101);
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department` (
  `dept_id` int NOT NULL,
  `dept_name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'cardiology');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor` (
  `d_id` int NOT NULL AUTO_INCREMENT,
  `d_firstName` varchar(20) DEFAULT NULL,
  `d_lastName` varchar(20) DEFAULT NULL,
  `d_joinDate` date DEFAULT NULL,
  `d_email` varchar(30) DEFAULT NULL,
  `d_password` varchar(600) DEFAULT NULL,
  `d_contact` varchar(15) DEFAULT NULL,
  `d_qualification` varchar(200) DEFAULT NULL,
  `d_status` bit(1) DEFAULT NULL,
  `d_address` varchar(400) DEFAULT NULL,
  `d_dob` date DEFAULT NULL,
  `d_gender` varchar(10) DEFAULT NULL,
  `dept_id` int DEFAULT NULL,
  `d_otp` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`d_id`),
  KEY `dept_id` (`dept_id`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'Rashmi','Sinha','2000-12-02','rashmi@gmail.com','$2a$10$l2EZseFPEy.8ztvlx/3ZOukKBAli10gpbCCceGkxwKpCsiQEhrt7q','123456789','MBBS',_binary '','Pune','2000-08-06','Female',1,'NULL'),(3,'sema','s','2001-12-11','sema@gmail.com','$2a$10$tpY836c3IHHiIRcbMzRVdupNJr0I0lMPWJ6MNxvT8r508fz3.p1nm','123456789','dentist',_binary '\0','delhi','1989-12-11','female',1,'NULL');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient` (
  `p_id` int NOT NULL AUTO_INCREMENT,
  `p_firstName` varchar(20) DEFAULT NULL,
  `p_lastName` varchar(20) DEFAULT NULL,
  `p_email` varchar(30) DEFAULT NULL,
  `p_password` varchar(600) DEFAULT NULL,
  `p_contact` varchar(15) DEFAULT NULL,
  `p_gender` varchar(10) DEFAULT NULL,
  `p_dob` date DEFAULT NULL,
  `p_bloodGroup` varchar(10) DEFAULT NULL,
  `p_address` varchar(400) DEFAULT NULL,
  `p_otp` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'Pragati','Kate','pragati@gmail.com','$2a$10$FyYMOh7hxO73OORmwz09iezkgXfZqHqNeajLT8wNfcE1vLM4AtaL6','9689980078','Female','1991-06-03','A+ve','Pune','Null'),(2,'Priti','p','priti@gmail.com','$2a$10$Jkd/MwwN/SS5.IZAHqUtJeT.dir.W0b3ETcHri7SsCA5HN2JlZNKy','123456789','Female','1994-02-01','b+ve','mumbai','NULL');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prescription` (
  `prc_id` int NOT NULL AUTO_INCREMENT,
  `prc_date` date DEFAULT NULL,
  `ap_id` int DEFAULT NULL,
  `medicines` varchar(6000) DEFAULT NULL,
  PRIMARY KEY (`prc_id`),
  KEY `ap_id` (`ap_id`),
  CONSTRAINT `prescription_ibfk_1` FOREIGN KEY (`ap_id`) REFERENCES `appointment` (`ap_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription`
--

LOCK TABLES `prescription` WRITE;
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` VALUES (1,'2022-04-06',1,'paracetamol, calpol');
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room` (
  `room_id` int NOT NULL,
  `room_type` varchar(30) DEFAULT NULL,
  `room_rate` double DEFAULT NULL,
  `room_status` bit(1) DEFAULT NULL,
  `ap_id` int DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `ap_id` (`ap_id`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`ap_id`) REFERENCES `appointment` (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (101,'AC',500,_binary '\0',1),(102,'NON-AC',200,_binary '',2);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-09 20:12:03
